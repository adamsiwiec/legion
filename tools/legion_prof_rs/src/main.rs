use rayon::prelude::*;
use std::io;

#[cfg(feature = "viewer")]
use legion_prof::backend::viewer;
#[cfg(feature = "server")]
use legion_prof::backend::viewer::StateDataSource;
use legion_prof::backend::{analyze, trace_viewer, visualize};
use legion_prof::serialize::deserialize;
use legion_prof::spy;
use legion_prof::state::{Records, SpyState, State, Timestamp};
#[cfg(feature = "client")]
use legion_prof_viewer::app;
#[cfg(feature = "client")]
use legion_prof_viewer::http::client::HTTPDataSource;
#[cfg(feature = "server")]
use legion_prof_viewer::http::server::DataSourceHTTPServer;

fn main() -> io::Result<()> {
    let matches = clap::App::new("Legion Prof")
        .about("Legion Prof: application profiler")
        .arg(
            clap::Arg::with_name("filenames")
                .help("input Legion Prof log filenames")
                .required(true)
                .multiple(true),
        )
        .arg(
            clap::Arg::with_name("output")
                .short("o")
                .long("output")
                .takes_value(true)
                .default_value("legion_prof")
                .help("output directory pathname"),
        )
        .arg(
            clap::Arg::with_name("start-trim")
                .long("start-trim")
                .takes_value(true)
                .help("start time in microseconds to trim the profile"),
        )
        .arg(
            clap::Arg::with_name("stop-trim")
                .long("stop-trim")
                .takes_value(true)
                .help("stop time in microseconds to trim the profile"),
        )
        .arg(
            clap::Arg::with_name("message-threshold")
                .long("message-threshold")
                .takes_value(true)
                .help("threshold for warning about message latencies in microseconds"),
        )
        .arg(
            clap::Arg::with_name("message-percentage")
                .long("message-percentage")
                .takes_value(true)
                .help("perentage of messages that must be over the threshold to trigger a warning"),
        )
        .arg(
            clap::Arg::with_name("force")
                .short("f")
                .long("force")
                .help("overwrite output directory if it exists"),
        )
        .arg(
            clap::Arg::with_name("statistics")
                .short("s")
                .long("statistics")
                .help("print statistics"),
        )
        .arg(
            clap::Arg::with_name("trace")
                .short("t")
                .long("trace-viewer")
                .help("emit JSON for Google Trace Viewer"),
        )
        .arg(
            clap::Arg::with_name("view")
                .long("view")
                .help("start interactive profile viewer"),
        )
        .arg(
            clap::Arg::with_name("serve")
                .long("serve")
                .help("start HTTP data source server"),
        )
        .arg(
            clap::Arg::with_name("fetch")
                .long("fetch")
                .help("fetch a remote server profile"),
        )
        .arg(
            clap::Arg::with_name("host")
                .long("host")
                .takes_value(true)
                .default_value("127.0.0.1")
                .help("set a host for remote fetch"),
        )
        .arg(
            clap::Arg::with_name("port")
                .long("port")
                .takes_value(true)
                .default_value("8080")
                .help("set a port for remote fetch"),
        )
        .get_matches();

    let filenames = matches.values_of_os("filenames").unwrap();
    let output = matches.value_of_os("output").unwrap();
    let host = matches.value_of("host").unwrap();
    let port = matches.value_of("port").unwrap().parse::<u16>().unwrap();

    let force = matches.is_present("force");
    let statistics = matches.is_present("stats");
    let trace = matches.is_present("trace");
    let view = matches.is_present("view");
    let serve = matches.is_present("serve");
    let fetch = matches.is_present("fetch");
    let start_trim = matches
        .value_of("start-trim")
        .map(|x| Timestamp::from_us(x.parse::<u64>().unwrap()));
    let stop_trim = matches
        .value_of("stop-trim")
        .map(|x| Timestamp::from_us(x.parse::<u64>().unwrap()));
    let message_threshold = matches
        .value_of("message-threshold")
        .map_or(1000.0, |x| x.parse::<f64>().unwrap());
    let message_percentage = matches
        .value_of("message-percentage")
        .map_or(5.0, |x| x.parse::<f64>().unwrap());

    let filenames: Vec<_> = filenames.collect();
    let records: Result<Vec<Records>, _> = filenames
        .par_iter()
        .map(|filename| {
            println!("Reading log file {:?}...", filename);
            deserialize(filename).map_or_else(
                |_| spy::serialize::deserialize(filename).map(Records::Spy),
                |r| Ok(Records::Prof(r)),
            )
        })
        .collect();
    let mut state = State::default();
    let mut spy_state = SpyState::default();
    for record in records? {
        match record {
            Records::Prof(r) => {
                println!("Matched {} objects", r.len());
                state.process_records(&r);
            }
            Records::Spy(r) => {
                println!("Matched {} objects", r.len());
                spy_state.process_spy_records(&r);
            }
        }
    }

    if !state.has_prof_data {
        println!("Nothing to do");
        return Ok(());
    }

    spy_state.postprocess_spy_records(&state);

    state.trim_time_range(start_trim, stop_trim);
    println!("Sorting time ranges");
    state.sort_time_range();
    state.check_message_latencies(message_threshold, message_percentage);
    if statistics {
        analyze::print_statistics(&state);
    } else if trace {
        trace_viewer::emit_trace(&state, output, force)?;
    } else if view {
        #[cfg(feature = "viewer")]
        {
            // initialize state
            state.assign_colors();
            viewer::start(state);
        }
    } else if serve {
        #[cfg(feature = "server")]
        {
            state.assign_colors();
            let server = DataSourceHTTPServer::new(
                port,
                host.to_string(),
                Box::new(StateDataSource::new(state)),
            );

            match server.create_server() {
                Err(e) => println!("{:?}", e),
                _ => (),
            }
        }
    } else if fetch {
        #[cfg(feature = "client")]
        {
            let fetch_source = HTTPDataSource::new(host.to_string(), port);
            app::start(Box::new(fetch_source), None);
        }
    } else {
        state.assign_colors();
        visualize::emit_interactive_visualization(&state, &spy_state, output, force)?;
    }

    Ok(())
}
